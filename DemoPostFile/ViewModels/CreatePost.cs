﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoPostFile.Model
{
    public class CreatePost
    {
        public string Title { get; set; }

        public IFormFile Thumbnail { get; set; }
    }
}
