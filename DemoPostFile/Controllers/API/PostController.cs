﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoPostFile.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoPostFile.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult> PostCategory([FromForm]CreatePost createPostRequest)
        {
            if (createPostRequest.Thumbnail != null && createPostRequest.Thumbnail.Length > 0)
            {
                return Ok($"Title: {createPostRequest.Title}, Co file");
            }

            return BadRequest($"Title: {createPostRequest.Title}, Hem co file");
        }
    }
}
